# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 11:05:59 2020

@author: Jishan
"""
import numpy as np

# Objective function
def cost_function(X, y, beta):
    cost=np.dot((np.dot(X,beta) - y).T,(np.dot(X,beta) - y))/(2*len(y))
    return cost

# Minimization of the objective function using stochastic gradient descent
def gradient_descent( X, y, beta, alpha, max_iterations):
    cost=[]
    for iteration in range(max_iterations):
        gradient = X.T.dot(X.dot(beta)-y) / len(y)
        
# Update beta
        beta = beta - alpha * gradient
        cost.append(cost_function(X, y, beta))   
    return beta, cost