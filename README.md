# Machine Learning
==================

### 01. Multivariate linear regression from scratch with stochastic gradient descent [[LinearRegression.py](Notebooks/LinearRegression.py)] [[main.py](Notebooks/main.py)]

#### Results:

I have implemented the stochastic gradient descent algorithm to perform multivariate linear regression using python language. Also, I have compared my results with the `sklearn.linear_model.SGDRegressor` of the `scikit-learn` Python library. I have used different values of the learning rate $`\alpha`$ for the sake of comparison. I would like to mention here that `scikit-learn` SGD has used $`\alpha`$ =0.0001 as a default. Computational time was also taken into consideration. The learning curve has ensured the convergence of the stochastic gradient descent algorithm. QSAR fish toxicity data Set was used in this assignment to predict the acute aquatic toxicity towards the fish Pimephales promelas (fathead minnow) on a set of 908 chemicals. LC50 variable, which is the concentration that causes death in 50% of test fish over a test duration of 96 hours, was used as a model response variable. The predictor variables were MLOGP (molecular properties), CIC0 (information indices), GATS1i (2D autocorrelations), NdssC (atom-type counts), NdsCH ((atom-type counts), SM1_Dz(Z) (2D matrix-based descriptors). I have standardized the predictor variables. 5-fold cross-validation method was used to avoid overfitting. I have evaluated my models using root mean squared error(RMSE), $`R^2`$, and computational time. I have found that for $`\alpha = 0.0001`$ SKlearn SGD took 0.0380 seconds, and my linear regression model took 19.85 seconds. For $`\alpha = 0.01,`$ linear regression model took 19.38 seconds. $`R^2`$ score for the `SKlearn` SGD was 0.53, and root mean squared error was 0.96. In contrast, linear regression had $`R^2`$ score 0.53, and root mean squared error 0.96 when $`\alpha = 0.0001`$. It is also noticeable that linear regression had the same $`R^2`$ score, and root mean squared error when $`\alpha = 0.01`$. Usually, too small values of $`\alpha`$ make algorithm slower, and too large values of 
$`\alpha`$ diverge the algorithm. Both `SKlearn` SGD and linear regression gave same $`R^2`$ score, and root mean squared error when $`\alpha = 0.0001`$. I can conclude that `SKlearn` SGD performed well compared to my implemented stochastic gradient descent algorithm in terms of computational time. 

### 02. Linear discriminant analysis (LDA) from scratch [[View Jupyter Notebook](Notebooks/LDA.ipynb)] 

#### Results:

We have implemented the above LDA method to classify randomly generated 1000 univariate samples along with 3 classes. Also, we have compared our results with the `LinearDiscriminantAnalysis` package of the `scikit-learn` Python library. Computational time was also taken into consideration. It was noticeable that our implemented algorithm and `LinearDiscriminantAnalysis` package of the `scikit-learn` failed to uncover the structure of the data. Because we randomly generated the data labels. 5-fold cross-validation was also implemented to avoid overfitting. Accuracy and F1 score for the `LinearDiscriminantAnalysis` package of the `scikit-learn` were 0.35, and 0.35 respectively. Our implemented `LDA` algorithm had the accuracy 0.35, and F1 score 0.35. The computational time for the `LinearDiscriminantAnalysis` package was 0.16 seconds on average. Our implemented algorithm had 0.18 seconds on average. We have seen that the accuracy and F1 score were identical for both cases. `LinearDiscriminantAnalysis` package of the `scikit-learn` did a slightly better job in terms of the computational time. We believe that maximizing the decision boundary to find the structure of the data was the most challenging part of this assignment. We believe that our implemented algorithm will do a better job on the real-world data sets. Implementation for a univariate random data gave us the flexibility to check our computations. Computational time also gave us a sense of the efficiency of our implemented algorithm. We have found that `LDA` is not flexible enough to classify the highly imbalanced data due to its normality assumption. It was a nice experience to understand the underlying mathematics while implementing the `LDA` algorithm.  

##### Contributors:
Jishan Ahmed, Upeksha Perera

### 03. A Feed forward neural network that is trained via Backpropogation from scratch [[View Jupyter Notebook](Notebooks/NeuralNetScratch.ipynb)] 

#### Results and discussion 
In this assignment, I have attempted to implement  a feed  forward neural network via `Backpropogation`. I have then trained my implementation  in order to predict whether a given car gets high or low gas mileage based on the `Auto` data set. I have created a binary variable that takes on a 1 for cars with gas mileage above the median, and a 0 for cars with gas mileage below the median. My main objective of this implementation was to explore the computational issues of the neural network. Since my data set had only 397 samples and 7 variables, I have considered input layer, one hidden layer, and output layer to design the neural network architecture. I have assigned 30 neurons for the hidden layer. Random weights values between -1 and 1 were assigned for each variable. 
In this implementation, I chose the `sigmoid` function, $`\sigma(x)=\frac{1}{1+e^{-x}}`$  as a activation function. To perform the feed forward, I have taken the `dot` product between $`X`$ and $`W^{(1)}`$ as $`z^{(2)}=XW^{(1)}`$. Next, I have evaluated the sigmoid function at $`z^{(2)}`$ as $`a=\sigma(z^{(2)})=\sigma(XW^{(1)})`$. I have then multiplied the resulting functional evaluation, $`a`$ by the weights $`W^{(2)}`$, i.e. $`z^{(3)}=aW^{(2)}`$. Last but not least, we have applied our activation function to $`z^{(3)}`$ which yields our estimate, $`\hat y=\sigma(z^{(3)})`$. The following two equations were used to implement the backpropagation algorithm,

$`\frac{\partial L}{\partial W^{(2)}}=a^T(-(y-\hat y))\sigma^{\prime}(z^{(3)})`$ and 
$`\frac{\partial L}{\partial W^{(1)}}=X^T(a^T(-(y-\hat y))\sigma^{\prime}(z^{(3)}))(W^{(2)})^{T}\sigma^{\prime}(z^{(2)})`$.

I have evaluated my implementation using computational time, and different values of regularization parameters `alpha`. From the following graph, we see that it took 8000 iterations to achieve the minimum loss for $`alpha=0.1`$. Computational time was also minimum as well. It just took 2.86 seconds to train our model. 

![diagram](/Graphs/NN_Loss3.PNG "The caption for my image")


##### References
<a id="1">[1]</a> 
https://www.youtube.com/watch?v=i94OvYb6noo&t=773s

<a id="2">[2]</a> 
https://iamtrask.github.io/2015/07/27/python-network-part2/

<a id="3">[3]</a> 
https://www.youtube.com/watch?v=GlcnxUlrtek&list=PLiaHhY2iBX9hdHaRr6b7XevZtgZRa1PoU&index=4

 
### 04. Decision trees, boosting, and bagging [[View Jupyter Notebook](Notebooks/FinalDecisionTreesRandomForsest.ipynb)] 

#### Results and discussion 
In this assignment, we have implemented the decision tree and the random forest algorithms to perform the regression analysis on the concrete compressive strength data set. The predictor variables are cement, blast furnace slag, fly ash, water, superplasticizer, coarse aggregate,  fine aggregate, and age. Our goal here is to predict the concrete compressive strength. It is challenging to tune the hyperparameters for the decision tree and the random forest algorithms. The decision tree and the random forest have a plethora of hyperparameters that require fine-tuning to deduce the best possible models that reduce the square root of the mean squared error (RMSE) as much as possible. In this assignment, we will focus on four specific hyperparameters: the number of maximum features, `ccp_alpha` (the pruning parameter), `max depth` (the maximum depth of the tree), `n_estimators` (the number of trees in the forest). We have used 5-fold cross-validation to find the optimal choice of the hyperparameters. We have implemented our regression analysis using the `DecisionTreeRegressor`, and the `RandomForestRegressor` packages of the `scikit-learn` python library. We have also attempted to tune hyperparameters using `sklearn.model_selection.GridSearchCV`. 

##### Decision tree regression 
We have implemented the decision tree using `DecisionTreeRegressor` package of the `scikit-learn` python library. The lowest RMSE was achieved for the max_features parameters: `auto` i.e all features were used while looking for the best split. The following figure indicates that a maximum depth of 16 and ccp_alpha value 0.01 are the best parameters for this data. It is also noticeable that the maximum depth exhibits a tendency to decrease for the  increased `ccp_alpha` value from 0 to 1. The maximum depth of around 2 was consistent for ccp_alpha value from 0.50 to 1.0.  We have used these optimal parameters and construct a new decision tree. In this optimal tree, around 70\% variance was explained by age, cement, and water. Also, the cross-validated RMSE associated with this regression tree is 11.73. 
![diagram](/Graphs/max_dtree.PNG "The caption for my image")

##### Random forest regression

We have implemented the random forest algorithm using the `RandomForestRegressor` packages of the `scikit-learn` python library. Since the random forests are based on multiple decision trees, the hyperparameters are pretty similar. The random forest algorithm developed by Breiman[[1]](#1) uses p/3 variables when building a random forest of regression trees. In our `scikit-learn` implementation, the lowest RMSE was achieved for the “max_features parameters: “auto”. The following figure shows that a maximum depth of 16 and `ccp_alpha` value 0.01 are the best parameters for this data. We also see that when `ccp_alpha` value is increasing, the maximum depth is decreasing gradually. The optimal number of estimators was 70. The results indicate that across all of the trees considered in the optimal random forest, age, cement, and superplasticizer, the three most important variables. Also, the cross-validated RMSE associated with this optimal random forest is 10.20. 
![diagram](/Graphs/max_rf.PNG "The caption for my image")
![diagram](/Graphs/estimator_rf.PNG "The caption for my image")
![diagram](/Graphs/importance.PNG "The caption for my image")


From the above discussion, it is obvious that the optimal random forest has achieved the lowest RMSE. Therefore, we can conclude that the random forest performed well to predict the concrete compressive strength. 

##### References
<a id="1">[1]</a> 
Breiman, L. Random Forests. Machine Learning 45, 5–32 (2001). `https://doi.org/10.1023/A:1010933404324`

##### Contributors:
Jishan Ahmed, Upeksha Perera


### 05. Exercises 9.5, 9.7, and 9.8 (Applied) from Introduction to Statistical Learning with Applications [[View Jupyter Notebook](Notebooks/Final_SVM_Exercises.ipynb)] 

#### Solution 

#### Exercise 9.5

We have seen that we can ﬁt an SVM with a non-linear kernel in order to perform classiﬁcation using a non-linear decision boundary. We will now see that we can also obtain a non-linear decision boundary by performing logistic regression using non-linear transformations of the features.
 
##### Generate a data set with n = 500 and p = 2 & Plot the observations, colored according to their class labels.

 <img src="https://live.staticflickr.com/65535/49632370518_3944915e3b_n.jpg" width="350" height="300" border="0" alt="" />
 
##### Fit a logistic regression model to the data, using X1 and X2 as predictors.

This gives accuracy and f1 score of 0.5.

 <img src="https://live.staticflickr.com/65535/49632918831_6a6a32ab59_z.jpg" width="350" height="300" border="0" alt="" />

##### Apply this model to the training data in order to obtain a predicted class label for each training observation. Plot the observations, colored according to the predicted class labels. 

<img src="https://live.staticflickr.com/65535/49633197297_7b00890524_n.jpg" width="350" height="300" border="0" alt="" />

##### Now ﬁt a logistic regression model to the data using non-linear functions of X1 and X2 as predictors.

<img src="https://live.staticflickr.com/65535/49632408953_0c9dfd9b52_n.jpg" width="350" height="300" border="0" alt="" />


Here we have used following transformations. x11= x1^4 and x22=x1*x2. Now the accuracy and f1 score is much higher.

<img src="https://live.staticflickr.com/65535/49632414118_6929b8c392_z.jpg" width="350" height="300" border="0" alt="" />

##### The decision boundary is obviously non-linear.

<img src="https://live.staticflickr.com/65535/49632943151_23b6347f02_n.jpg" width="350" height="300" border="0" alt="" />
 
##### Fit a support vector classiﬁer to the data with X1 and X2 as predictors. 
SVM classiﬁer is fitted to the original data. Here the linear kernel is used. Accuracy is almost 1.

<img src="https://live.staticflickr.com/65535/49633222447_44d0e482a1_n.jpg" width="350" height="300" border="0" alt="" />
  
#####  Fit a SVM using a non-linear kernel to the data. Obtain a class prediction for each training observation. 
##### SVM classiﬁer is fitted to the transformed data. Here the sigmoid kernel is used. Accuracy and F1 score is also calculated (0.62).

<img src="https://live.staticflickr.com/65535/49633362897_e4c46c8f8d_n.jpg" width="350" height="300" border="0" alt="" />
 
##### SVM classiﬁer is fitted to the transformed data. Here the polynomial kernel is used. Accuracy and F1 score is much higher (0.90)

<img src="https://live.staticflickr.com/65535/49632570353_dfe28e604b_n.jpg" width="350" height="300" border="0" alt="" />

##### SVM classiﬁer is fitted to the transformed data. Here the radial kernel is used. Accuracy and F1 score is high (0.9)

<img src="https://live.staticflickr.com/65535/49633091361_b10d7f07d1_n.jpg" width="350" height="300" border="0" alt="" />


#### Exercise 9.7

#### (a) Create a binary variable

```
def convert_mpg_to_binary(x):
    if x > med_mpg:
        return 1
    else:
        return 0

df1["mpg_median"] = df1["mpg"].apply(convert_mpg_to_binary)
```

#### (b) Fit a support vector classiﬁer

We have trained our support vector classiﬁer(SVC) without scaling the data to explore the computaional issues. It turns out that cost, and computational time are associated with the data scaling.SVC requires more computational time and lower cost value for the unscaled data. In contrast, SVC with scaled data requires higher cost value and lower computational time. Both cases, we have used `linear` kernel. Implemented part of this SVC is given below:

```
ccp_a=[0.01, 0.1, 1, 5, 10, 100]
mse_a=[]
start_time = time.time()
for i in range(len(ccp_a)):
        clf_t = svm.SVC(kernel='linear',C=ccp_a[i])
        auc_error =cross_val_score(clf_t,X_new,y,scoring='roc_auc', cv=10)
        print(f'When the value of the cost is:{ccp_a[i]}')
        print(f'Support Vector 5-fold cross validation ROC-AUC score: {np.mean(auc_error)}\n')
        mse_a.append(np.mean(auc_error))
end_time = time.time()  
print ("Computational time without scaling  = " +str(end_time - start_time) )
```

We found that when the value of the cost (C) was 1,  the SVC 10-fold cross validation ROC-AUC score was 1.0 for the unscaled data. The total computational time was 24.16 seconds. When the value of the cost (C) was 100,  the SVC 10-fold cross validation ROC-AUC score was 1.0 for the scaled data. In this case, the total computational time was 0.51 seconds. We can aslo see the performaces of the SVC for different values of `cost` from the following graph.


![diagram](/Graphs/Capture.PNG "The caption for my image")


#### (c) SVMs with radial and polynomial basis kernels, with different values of `gamma` and `degree` and `cost`

We have also used `GridSearchCV` package of the `scikit-learn` python library to tune hyperparameters. It was taking long time to perfrom the `GridSearchCV` for the unscaled data. We think that it is important to scale the data before running `GridSearchCV`. It is extremely slow for the unscaled data. We have implemented the following `GridSearchCV` to find the best choice of `Kerenel`, `gamma` and `degree` and `cost`. 

```
gs = svm.SVC()
parameter_space =[{'kernel':['rbf'],'gamma':[0.01, 0.1, 1],'C' :[0.1,1,10,100]}, 
                  {'kernel':['poly'], 'gamma':[0.01, 0.1, 1],'C' :[0.1, 1,10,100],'degree':[2, 3]}]
                    
clf_gs = GridSearchCV(gs, parameter_space,cv=3,n_jobs=-1)
start_time_gs = time.time()
clf_gs.fit(XScaledDF, y)
end_time_gs = time.time()  
print ("Computational time of the training with scaling = " +str(end_time_gs - start_time_gs) )
print("Best Score: {}".format(clf_gs.best_score_))
print("Best params: {}".format(clf_gs.best_params_))
```

We have found that the following optimal parameters.
```
Best params: {'C': 100, 'gamma': 0.01, 'kernel': 'rbf'}
```

The computational time was 1.17 seconds. We have implemented the `GridSearchCV` 
to find the optimal choice of `gamma` and `degree` and `cost` for the radial and polynomial basis kernels respectively. 
For the radial basis kernel, we got the following results.
```
Best params: {'C': 10, 'gamma': 0.01, 'kernel': 'rbf'}
```
The computational time was 4.20 seconds, and the AUC score was 0.95. For the polynomial basis kernel, we obtained the following results,
```
Best params: {'C': 5, 'degree': 3, 'gamma': 1, 'kernel': 'poly'}
```
It took  0.34 seconds, and the AUC score was 0.917. We see that `poly` kernel performed well in terms of computational time. On the other hand, `rbf` kernel achieved highest AUC score.


#### (d) Plot the ﬁrst and fourth variables

The following graph shows the comparison of different SVM classifiers.

![diagram](/Graphs/Capture2.PNG "The caption for my image")

We have only considered the first 2 features of the `Auto` data set. It interesting that all models are showing linear decision boundaries (intersecting hyperplanes). We can conclude that the shape of the decision boundaries depends on the kind of kernel and its parameters.


#### Exercise 9.8

#### (a) Create a training set containing a random sample of 800 observations, and a test set containing the remaining observations
 We have used train_size parameter to split the OJ dataset with 800 observations as training data and remaining 271 as test data.

#### (b) Fit a support vector classifier to the training data using cost=0.01, with Purchase as the response and the other variables as predictors. Use the summary() function to produce summary statistics, and describe the results obtained.
 Scaled and normalized data using standard scaler and Pandas data frame to provide Purchase as the response and other variables as predictors. Dropped Store_7 with boolean value.
In order to describe data and provide summary statistics, utilized SVM.get_params, classification report and confusion matrix as an equivalent of summary() function in R library. 
Used C parameter in SVC to provide the cost value of 0.01. Performed 5 fold cross validation using cost = 0.01 and obtained the precision, f1-score, accuracy, avg results.

bound method BaseEstimator.get_params of SVC(C=0.01, break_ties=False, cache_size=200, class_weight=None, coef0=0.0,
    decision_function_shape='ovr', degree=3, gamma='scale', kernel='linear',
    max_iter=-1, probability=False, random_state=None, shrinking=True,
    tol=0.001, verbose=False)>
Training error rate 30.625000000000004
Test error rate 31.874999999999996
Confusion matrix [[97  2]
 [49 12]]
              precision    recall  f1-score   support

          CH       0.66      0.98      0.79        99
          MM       0.86      0.20      0.32        61

    accuracy                           0.68       160
   macro avg       0.76      0.59      0.56       160
weighted avg       0.74      0.68      0.61       160

#### (c) What are the training and test error rates?
Found a training error rate of: 31.04 %
And testing error rate of: 35.93 %

#### (d)	Use the tune() function to select an optimal cost. Consider values in the range 0.01 to 10.
Used GridSearchCV to tune the parameters to obtain the optimal cost. Performed 7 fold cross validation with C values in the range of 'C': [0.01, 0.1, 0.5, 1, 2, 5, 10] 
Tuning shows that optimal cost is: 0.5 for the linear kernel
{'C': 0.5, 'kernel': 'linear'}

#### (e) Compute the training and test error rates using this new value for cost.
mean_test_score': [0.58255409, 0.81564398, 0.82808341, 0.82654768, 0.82499488, 0.82654768, 0.82808341],


#### (f) Repeat parts (b) through (e) using a support vector machine with a radial kernel. Use the default value for gamma.
Repeating the above steps with radial kernel parameter rbf. and provided default value for gamma = 'scaled' gave following results:

 'mean_test_score': array([0.67155022, 0.67155022, 0.67155022, 0.67155022, 0.67155022,
        0.67155022, 0.67155022]),

  precision    recall  f1-score   support

          CH       0.71      1.00      0.83        91
          MM       1.00      0.00      0.00        37

    accuracy                           0.71       128
   macro avg       0.86      0.50      0.42       128
weighted avg       0.79      0.71      0.59       128

Tuning shows that optimal cost is 0.01 for radial kernel
{'C': 0.01, 'gamma': 'scale', 'kernel': 'rbf'}

#### (g) Repeat parts (b) through (e) using a support vector machine with a polynomial kernel. Set degree=2.
Repeating the above steps with polynomial kernel parameter poly. and degree of 2 and 3 provided gave the following results:

'mean_test_score': [0.67073171, 0.67073171, 0.67073171, 0.67073171, 0.67073171,
        0.67073171, 0.67073171, 0.67073171, 0.67073171, 0.67073171,
        0.67073171, 0.67073171, 0.67073171, 0.67073171]
        
  precision    recall  f1-score   support

          CH       0.65      1.00      0.79        66
          MM       1.00      0.00      0.00        36

    accuracy                           0.65       102
   macro avg       0.82      0.50      0.39       102
weighted avg       0.77      0.65      0.51       102


Tuning shows that optimal cost is 0.01 with degree 2
 {'C': 0.01, 'degree': 2, 'gamma': 'scale', 'kernel': 'poly'}


#### (h) Overall, which approach seems to give the best results on this data?
Overall, radial kernel seems to be provide better results with lower C values and obtained macro avg of 0.86 and weighted avg of 0.79

##### Contributors:
Jishan Ahmed, Upeksha Perera, and Karthik Jagilinki
